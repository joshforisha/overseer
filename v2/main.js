const $ = (sel) => document.querySelector(sel)
const $$ = (sel) => document.querySelectorAll(sel)

const alertsRegion = $('#Alerts')

// Functions -------------------------------------------------------------------

function showAlert({ type }, text) {
  const alert = createAlert({ type }, text)
  alertsRegion.appendChild(alert)
  setTimeout(() => {
    alertsRegion.removeChild(alert)
  }, 5000)
}

function createAlert({ type }, text) {
  const alert = document.createElement('div')
  alert.classList.add('alert')
  alert.classList.add(`-${type}`)
  alert.textContent = text
  return alert
}

// Initialization --------------------------------------------------------------

showAlert({ type: 'success' }, 'Loaded!')
