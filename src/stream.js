export class Stream {
  constructor (subscribe) {
    this.subscribe = subscribe
  }

  static from (values) {
    return new Stream(stream => {
      values.forEach(value => stream.next(value))
      stream.complete()
      return () => {
        console.log('Unsubscribed')
      }
    })
  }

  static fromEvent (element, eventType) {
    // TODO
  }
}

export const $ = Stream

// Testing -----------------------------------------------------------------------------

const numbers$ = Stream.from([0, 1, 2, 3, 4])
const unsubNumbers = numbers$.subscribe({
  complete: () => console.info('Done'),
  error: console.error,
  next: console.log
})

window.setTimeout(unsubNumbers, 500)
