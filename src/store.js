export const store = {
  time: Date.now()
}

const subscribers = {}

export function subscribe (subscriber, keys = []) {
  const id = Date.now()
  keys.forEach(key => {
    if (!(key in store)) console.warn(`No store key "${key}"`)
  })
  subscribers[id] = [subscriber, keys]
  return () => {
    if (!(id in subscribers)) return console.warn(`Unnecessary unsubscribe (${id})`)
    delete subscribers[id]
  }
}

export function update (fn) {
  const changes = fn(store)
  const changedKeys = Object.keys(changes)
  Object.entries(fn(store)).forEach(([key, value]) => {
    if (!(key in store)) return console.warn(`No store key "${key}"`)
    store[key] = value
  })
  Object.values(subscribers)
    .filter(([, subscribedKeys]) =>
      subscribedKeys === [] ||
      changedKeys.some(k => subscribedKeys.includes(k))
    )
    .forEach(([subscriber]) => {
      subscriber(store)
    })
}
