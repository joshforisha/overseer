const map = new Array(7).fill(0).map(() => new Array(9).fill(false))
map[3][4] = true

const activeCoords = [[3, 4]]
while (activeCoords.length < 16) {
  const [x, y] = activeCoords[Math.floor(Math.random() * activeCoords.length)]
  const opts = []
  if (x > 0 && !map[x - 1][y]) opts.push([x - 1, y])
  if (y < 8 && !map[x][y + 1]) opts.push([x, y + 1])
  if (x < 6 && !map[x + 1][y]) opts.push([x + 1, y])
  if (y > 0 && !map[x][y - 1]) opts.push([x, y - 1])
  if (opts.length > 0) {
    const [i, j] = opts[Math.floor(Math.random() * opts.length)]
    map[i][j] = true
    activeCoords.push([i, j])
  }
}

activeCoords.forEach(([x, y]) => {
  const letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G'][x]
  const number = y + 1
  document.getElementById(`Sector${letter}${number}`).classList.add('-active')
})

// Canvas experiment

const canvas = document.createElement('canvas')
canvas.setAttribute('id', 'Scanlines')
canvas.height = window.innerHeight
canvas.width = window.innerWidth

const gl = canvas.getContext('webgl2')
const vertexShader = gl.createShader(gl.VERTEX_SHADER)
gl.shaderSource(vertexShader, `
precision mediump float;

attribute vec2 a_position;
attribute vec2 a_texcoord;

varying vec2 v_texcoord;

void main() {
  gl_Position = vec4(a_position, 0.0, 1.0);
  v_texcoord = a_texcoord;
}
`)
gl.compileShader(vertexShader)

const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER)
gl.shaderSource(fragmentShader, `
precision mediump float;

#define BRIGHT_BOOST 1.5
#define DILATION 1.0
#define GAMMA_INPUT 2.4
#define GAMMA_OUTPUT 1.1
#define MASK_SIZE 1.0
#define MASK_STAGGER 0
#define MASK_STRENGTH 0.3
#define MASK_DOT_HEIGHT 1.0
#define MASK_DOT_WIDTH 1
#define SCANLINE_BEAM_WIDTH_MAX 1.5
#define SCANLINE_BEAM_WIDTH_MIN 1.5
#define SCANLINE_BRIGHT_MAX 0.65
#define SCANLINE_BRIGHT_MIN 0.35
#define SCANLINE_CUTOFF 400.0
#define SCANLINE_STRENGTH 1.0
#define SHARPNESS_H 0.5
#define SHARPNESS_V 1.0

#define PI 3.141592653589

uniform vec2 u_resolution;

void main() {
  float my = mod(gl_FragCoord.y, 4.0) / 4.0;
  gl_FragColor = vec4(1.0, 1.0, 1.0, my < 0.5 ? 0.0 : 1.0);
}
`)
gl.compileShader(fragmentShader)

const program = gl.createProgram()
gl.attachShader(program, vertexShader)
gl.attachShader(program, fragmentShader)
gl.linkProgram(program)
gl.useProgram(program)
gl.deleteShader(vertexShader)
gl.deleteShader(fragmentShader)

const texcoords = new Float32Array([0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1])
const coordLocation = gl.getAttribLocation(program, 'a_texcoord')
gl.bindBuffer(gl.ARRAY_BUFFER, gl.createBuffer())
gl.bufferData(gl.ARRAY_BUFFER, texcoords, gl.STATIC_DRAW)
gl.enableVertexAttribArray(coordLocation)
gl.vertexAttribPointer(coordLocation, 2, gl.FLOAT, false, 0, 0)

const positions = new Float32Array([-1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1])
const posLocation = gl.getAttribLocation(program, 'a_position')
gl.bindBuffer(gl.ARRAY_BUFFER, gl.createBuffer())
gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW)
gl.enableVertexAttribArray(posLocation)
gl.vertexAttribPointer(posLocation, 2, gl.FLOAT, false, 0, 0)

gl.uniform2f(gl.getUniformLocation(program, 'u_resolution'), window.innerWidth, window.innerHeight)

gl.drawArrays(gl.TRIANGLES, 0, 6)

document.body.appendChild(canvas)
